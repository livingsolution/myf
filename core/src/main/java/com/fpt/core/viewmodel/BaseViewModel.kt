/*
 * Created by HiepNV12 on 9/11/18 1:04 PM
 * Copyright (c) 2018 FSOFT. All rights reserved.
 * Last modified 9/11/18 1:04 PM
 */

package com.fpt.core.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel : ViewModel() {
    val compositeDisposable = CompositeDisposable()
    var observerError = MutableLiveData<Throwable>()

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }
}