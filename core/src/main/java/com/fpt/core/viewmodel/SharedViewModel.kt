/*
 * Created by HiepNV12 on 9/17/18 9:50 AM
 * Copyright (c) 2018 FSOFT. All rights reserved.
 * Last modified 9/14/18 8:46 AM
 */

package com.fpt.core.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

class SharedViewModel<T> : ViewModel() {
    private val selected = MutableLiveData<T>()

    fun select(item: T) {
        selected.value = item
    }

    fun getSelected(): LiveData<T> {
        return selected
    }
}
