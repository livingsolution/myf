/*
 * Created by HiepNV12 on 9/10/18 1:09 PM
 * Copyright (c) 2018 by FSOFT. All rights reserved.
 * Last modified 9/10/18 9:10 AM
 */

package com.fpt.core.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable {
}