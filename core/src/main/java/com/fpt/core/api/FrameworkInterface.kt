/*
 * Created by HiepNV12 on 9/12/18 10:44 AM
 * Copyright (c) 2018 FSOFT. All rights reserved.
 * Last modified 9/12/18 10:44 AM
 */

package com.fpt.core.api

import retrofit2.Retrofit

interface FrameworkInterface {
    /**
     * Provides an instance of Retrofit with it's base URL
     */
    abstract fun getRetrofitApiInstance(): Retrofit
}