/*
 * Created by HiepNV12 on 9/11/18 1:35 PM
 * Copyright (c) 2018 FSOFT. All rights reserved.
 * Last modified 9/11/18 11:22 AM
 */

package com.fpt.myfsoft.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.fpt.myfsoft.modules.main.model.SampleResponse
import com.fpt.myfsoft.db.DATABASE.DATABASE_SAMPLE_VERSION
import com.fpt.myfsoft.db.DATABASE.DATABASE_SAMPLE
import com.fpt.myfsoft.modules.main.room.SampleDao

@Database(entities = [SampleResponse::class], version = DATABASE_SAMPLE_VERSION, exportSchema = false)
abstract class SampleDatabase : RoomDatabase() {

    abstract fun sampleDao(): SampleDao

    companion object {

        @Volatile
        private var INSTANCE: SampleDatabase? = null

        fun getInstance(context: Context): SampleDatabase = INSTANCE
                ?: synchronized(this) {
            INSTANCE
                    ?: buildMoviesDatabase(context).also { INSTANCE = it }
        }

        private fun buildMoviesDatabase(context: Context) = Room.databaseBuilder(context, SampleDatabase::class.java, DATABASE_SAMPLE).build()
    }
}
