/*
 * Created by HiepNV12 on 9/11/18 11:01 AM
 * Copyright (c) 2018 FSOFT. All rights reserved.
 * Last modified 9/11/18 11:01 AM
 */

package com.fpt.myfsoft.db

object DATABASE {
    const val DATABASE_SAMPLE = "sample.db"
    const val DATABASE_SAMPLE_VERSION = 1
    const val TABLE_SAMPLE = "git"

    const val SELECT_SAMPLE = "SELECT * FROM $TABLE_SAMPLE"

    const val PAGE_SIZE = 20
}