/*
 * Created by HiepNV12 on 9/10/18 1:11 PM
 * Copyright (c) 2018 FSOFT. All rights reserved.
 * Last modified 9/10/18 12:54 PM
 */

package com.fpt.myfsoft

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.view.Menu
import android.view.MenuItem
import com.fpt.core.ktextensions.replaceFragment
import com.fpt.myfsoft.base.activitys.BaseActivity
import com.fpt.myfsoft.modules.main.ui.fragments.MainFragment
import kotlinx.android.synthetic.main.activity_main.*
import com.fpt.core.base.BottomNavigationBehavior
import android.support.design.widget.CoordinatorLayout



class MainActivity : BaseActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    override fun initLayout(): Int {
        return R.layout.activity_main
    }

    override fun configureLogic(savedInstanceState: Bundle?) {
        setSupportActionBar(toolbar)
        replaceFragment(MainFragment(), R.id.container, null)
    }

    override fun setEvents() {
        navigation.setOnNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.navigation_home -> {
                return true
            }
            R.id.navigation_dashboard -> {
                return true
            }
            R.id.navigation_notifications -> {
                return true
            }
            else -> {
                return false
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
