/*
 * Created by HiepNV12 on 9/11/18 11:00 AM
 * Copyright (c) 2018 FSOFT. All rights reserved.
 * Last modified 9/11/18 11:00 AM
 */

package com.fpt.myfsoft.modules.main.room

import android.arch.paging.DataSource
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.fpt.myfsoft.modules.main.model.SampleResponse
import com.fpt.myfsoft.db.DATABASE.SELECT_SAMPLE

@Dao
interface SampleDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movieList: List<SampleResponse>)

    @Query(SELECT_SAMPLE)
    fun allSample(): DataSource.Factory<Int, SampleResponse>
}