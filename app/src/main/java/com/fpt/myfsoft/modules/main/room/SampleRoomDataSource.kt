/*
 * Created by HiepNV12 on 9/11/18 11:05 AM
 * Copyright (c) 2018 FSOFT. All rights reserved.
 * Last modified 9/11/18 11:05 AM
 */

package com.fpt.myfsoft.modules.main.room

import com.fpt.myfsoft.OpenForTesting
import com.fpt.myfsoft.modules.main.model.SampleResponse

@OpenForTesting
open class SampleRoomDataSource(private val sampleDao: SampleDao) {

    fun storeSample(sampleList: List<SampleResponse>) = sampleDao.insert(sampleList)

    fun getSample() = sampleDao.allSample()
}