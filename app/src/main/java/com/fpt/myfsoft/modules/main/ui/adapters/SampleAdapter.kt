/*
 * Created by HiepNV12 on 9/11/18 11:34 AM
 * Copyright (c) 2018 FSOFT. All rights reserved.
 * Last modified 9/11/18 11:34 AM
 */

package com.fpt.myfsoft.modules.main.ui.adapters

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.fpt.core.ktextensions.inflate
import com.fpt.myfsoft.R
import com.fpt.myfsoft.modules.main.model.SampleResponse
import kotlinx.android.synthetic.main.item_sample.view.*

class SampleAdapter: PagedListAdapter<SampleResponse, SampleAdapter.SampleViewHolder>(sampleDiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            SampleViewHolder(parent.inflate(R.layout.item_sample))

    override fun onBindViewHolder(holder: SampleViewHolder, position: Int) {
        val item = getItem(position)
        item?.let {
            holder.render(it)
        }
    }

    class SampleViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun render(sampleResponse: SampleResponse) = itemView.run {
            textView.text = sampleResponse.name
            textView2.text = sampleResponse.stars.toString()
            textView3.text = sampleResponse.author
        }
    }

    companion object {
        private val sampleDiffCallback = object : DiffUtil.ItemCallback<SampleResponse>() {
            override fun areItemsTheSame(oldItem: SampleResponse, newItem: SampleResponse): Boolean {
                return oldItem.author == newItem.author
            }

            override fun areContentsTheSame(oldItem: SampleResponse, newItem: SampleResponse): Boolean {
                return oldItem == newItem
            }
        }
    }
}