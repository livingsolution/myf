/*
 * Created by HiepNV12 on 9/11/18 11:10 AM
 * Copyright (c) 2018 FSOFT. All rights reserved.
 * Last modified 9/11/18 11:10 AM
 */

package com.fpt.myfsoft.modules.main.repository

import android.annotation.SuppressLint
import android.arch.paging.PagedList
import com.fpt.core.ktextensions.applySchedulers
import com.fpt.myfsoft.api.MyFsoftApi
import com.fpt.myfsoft.modules.main.model.SampleResponse
import com.fpt.myfsoft.modules.main.room.SampleRoomDataSource
import timber.log.Timber

class PageListBoundaryCallback(private val myFsoftApi: MyFsoftApi,
                               private val sampleRoomDataSource: SampleRoomDataSource) : PagedList.BoundaryCallback<SampleResponse>() {

    private var isRequestRunning = false
    private var requestedPage = 1

    override fun onZeroItemsLoaded() {
        Timber.tag(TAG).i("onZeroItemsLoaded")
        fetchAndStoreSample()
    }


    override fun onItemAtEndLoaded(itemAtEnd: SampleResponse) {
        Timber.tag(TAG).i("onItemAtEndLoaded")
        fetchAndStoreSample()
    }

    /**
     * @method fech api and save to db
     */
    @SuppressLint("CheckResult")
    private fun fetchAndStoreSample() {
        if (isRequestRunning) return

        isRequestRunning = true
        myFsoftApi.onSample()
                .map { movieApiList ->
                    movieApiList.map {
                        it
                    }
                }
                .doOnSuccess { listMovie ->
                    if (listMovie.isNotEmpty()) {
                        sampleRoomDataSource.storeSample(listMovie)
                        Timber.tag(TAG).i("Inserted: ${listMovie.size}")
                    } else {
                        Timber.tag(TAG).i("No Inserted")
                    }
                    requestedPage++
                }
                .applySchedulers()
                .doFinally { isRequestRunning = false }
                .subscribe(
                        {
                            Timber.tag(TAG).i("Movies Completed")
                        }, { it.printStackTrace() })

    }

    companion object {
        private const val TAG: String = "PageListMovieBoundary "
    }
}