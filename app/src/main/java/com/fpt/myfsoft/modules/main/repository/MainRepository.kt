/*
 * Created by HiepNV12 on 9/10/18 1:11 PM
 * Copyright (c) 2018 by FSOFT. All rights reserved.
 * Last modified 9/10/18 12:54 PM
 */

package com.fpt.myfsoft.modules.main.repository

import android.arch.paging.PagedList
import android.arch.paging.RxPagedListBuilder
import com.fpt.myfsoft.OpenForTesting
import com.fpt.myfsoft.api.MyFsoftApi
import com.fpt.myfsoft.modules.main.model.SampleResponse
import com.fpt.myfsoft.modules.main.room.SampleRoomDataSource
import com.fpt.myfsoft.db.DATABASE.PAGE_SIZE
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Repository that handles Repo instances.
 */
@Singleton
@OpenForTesting
class MainRepository @Inject constructor(private val myFsoftApi: MyFsoftApi, private val sampleRoomDataSource: SampleRoomDataSource) {
    fun fechSample(): Observable<PagedList<SampleResponse>> = RxPagedListBuilder(sampleRoomDataSource.getSample(), PAGE_SIZE)
            .setBoundaryCallback(PageListBoundaryCallback(myFsoftApi, sampleRoomDataSource))
            .buildObservable()
}