package com.fpt.myfsoft.modules.main.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.fpt.myfsoft.db.DATABASE.TABLE_SAMPLE
import com.google.gson.annotations.SerializedName

@Entity(tableName = TABLE_SAMPLE)
data class SampleResponse(

        @field:SerializedName("forks")
        val forks: Int,

        @PrimaryKey
        @field:SerializedName("author")
        val author: String,

        @field:SerializedName("name")
        val name: String,

        @field:SerializedName("description")
        val description: String,

        @field:SerializedName("language")
        val language: String,

        @field:SerializedName("stars")
        val stars: Int,

        @field:SerializedName("url")
        val url: String,

        @field:SerializedName("currentPeriodStars")
        val currentPeriodStars: Int
)