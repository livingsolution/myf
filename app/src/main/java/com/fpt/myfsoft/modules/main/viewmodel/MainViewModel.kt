/*
 * Created by HiepNV12 on 9/10/18 1:08 PM
 * Copyright (c) 2018 by FSOFT. All rights reserved.
 * Last modified 9/10/18 12:54 PM
 */

package com.fpt.myfsoft.modules.main.viewmodel

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PagedList
import com.fpt.core.ktextensions.addToCompositeDisposable
import com.fpt.core.viewmodel.BaseViewModel
import com.fpt.myfsoft.modules.main.model.SampleResponse
import com.fpt.myfsoft.modules.main.repository.MainRepository
import javax.inject.Inject

@SuppressLint("CheckResult")
class MainViewModel @Inject constructor(private val mainRepository: MainRepository) : BaseViewModel() {
    var pagedListSample = MutableLiveData<PagedList<SampleResponse>>()


    fun getSample() {
        mainRepository.fechSample()
                .addToCompositeDisposable(this)
                .subscribe(
                        {
                            pagedListSample.postValue(it)
                        },
                        {
                            it.printStackTrace()
                            observerError.postValue(it)
                        })
    }
}