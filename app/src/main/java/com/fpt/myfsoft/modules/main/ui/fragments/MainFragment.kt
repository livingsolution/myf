/*
 * Created by HiepNV12 on 9/10/18 1:11 PM
 * Copyright (c) 2018 FSOFT. All rights reserved.
 * Last modified 9/10/18 12:54 PM
 */

package com.fpt.myfsoft.modules.main.ui.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.fpt.myfsoft.R
import com.fpt.myfsoft.base.fragments.BaseFragment
import com.fpt.myfsoft.modules.main.model.SampleResponse
import com.fpt.myfsoft.modules.main.ui.adapters.SampleAdapter
import com.fpt.myfsoft.modules.main.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : BaseFragment() {
    private val viewModel: MainViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
    }
    private val sampleAdapter: SampleAdapter by lazy {
        SampleAdapter()
    }

    override fun initLayout(): Int {
        return R.layout.fragment_main
    }

    override fun initView(view: View) {
        recyclear.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = sampleAdapter
        }
    }

    override fun configureLogic() {
    }

    override fun configureViewModel() {
        viewModel.getSample()
        viewModel.pagedListSample.observe(this, Observer<PagedList<SampleResponse>> {
            Log.d(MainFragment::class::java.name, "Sample: ${it?.size}")
            sampleAdapter.submitList(it)
        })
        viewModel.observerError.observe(this, Observer<Throwable> {
            onError(it)
        })
    }

    override fun onDestroyViews() {
    }

    override fun onError(throwable: Throwable?) {

    }
}