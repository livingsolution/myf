/*
 * Created by HiepNV12 on 9/10/18 1:13 PM
 * Copyright (c) 2018 by FSOFT. All rights reserved.
 * Last modified 9/10/18 12:38 PM
 */

package com.fpt.myfsoft.api

import com.fpt.myfsoft.api.base.MyFsoftFramework
import com.fpt.myfsoft.api.endpoint.MyFsoftEndpoint
import com.fpt.myfsoft.modules.main.model.SampleResponse
import io.reactivex.Single

open class MyFsoftApi {
    /**
     * Lazily evaluates an instance of [MyFsoftEndpoint].
     */
    private val endpoints: MyFsoftEndpoint by lazy {
        MyFsoftFramework.retrofitApiInstance
                .create(MyFsoftEndpoint::class.java)
    }

    fun onSample(): Single<List<SampleResponse>> {
        return endpoints.onTrending("https://github-trending-api.now.sh/repositories?language=swift&since=weekly")
    }
}