/*
 * Created by HiepNV12 on 9/10/18 1:13 PM
 * Copyright (c) 2018 by FSOFT. All rights reserved.
 * Last modified 9/10/18 12:33 PM
 */

package com.fpt.myfsoft.api.endpoint

import com.fpt.myfsoft.modules.main.model.SampleResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Url

interface MyFsoftEndpoint {
    @GET()
    fun onTrending(@Url url: String): Single<List<SampleResponse>>
}