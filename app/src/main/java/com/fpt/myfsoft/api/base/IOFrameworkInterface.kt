/*
 * Created by HiepNV12 on 9/10/18 1:13 PM
 * Copyright (c) 2018 by FSOFT. All rights reserved.
 * Last modified 9/10/18 12:38 PM
 */

package com.fpt.myfsoft.api.base

import com.fpt.core.api.FrameworkInterface
import retrofit2.Retrofit

interface IOFrameworkInterface: FrameworkInterface {
}