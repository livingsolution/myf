/*
 * Created by HiepNV12 on 9/10/18 1:13 PM
 * Copyright (c) 2018 by FSOFT. All rights reserved.
 * Last modified 9/10/18 12:38 PM
 */

package com.fpt.myfsoft.api.base

import retrofit2.Retrofit

object MyFsoftFramework {
    private lateinit var retrofitInterface: IOFrameworkInterface

    val retrofitApiInstance: Retrofit
        get() = retrofitInterface.getRetrofitApiInstance()


    fun init(frameworkInterface: IOFrameworkInterface) {
        retrofitInterface = frameworkInterface
    }
}