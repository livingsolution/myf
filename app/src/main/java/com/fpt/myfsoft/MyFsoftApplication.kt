/*
 * Created by HiepNV12 on 9/10/18 1:11 PM
 * Copyright (c) 2018 by FSOFT. All rights reserved.
 * Last modified 9/10/18 12:41 PM
 */

package com.fpt.myfsoft

import android.app.Activity
import android.support.multidex.MultiDexApplication
import com.fpt.myfsoft.api.base.IOFrameworkInterface
import com.fpt.myfsoft.api.base.MyFsoftFramework
import com.fpt.myfsoft.di.AppInjector
import dagger.Lazy
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import retrofit2.Retrofit
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

class MyFsoftApplication: MultiDexApplication(), HasActivityInjector, IOFrameworkInterface {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>
    @field:[Inject Named("api")]
    lateinit var retrofitApi: Lazy<Retrofit>

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        // Inject into Application
        AppInjector.init(this)
        // Pass objects API
        MyFsoftFramework.init(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingAndroidInjector

    override fun getRetrofitApiInstance(): Retrofit  = retrofitApi.get()
}