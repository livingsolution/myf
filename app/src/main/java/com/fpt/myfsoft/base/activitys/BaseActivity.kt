/*
 * Created by HiepNV12 on 9/10/18 1:12 PM
 * Copyright (c) 2018 by FSOFT. All rights reserved.
 * Last modified 9/10/18 12:28 PM
 */

package com.fpt.myfsoft.base.activitys

import android.arch.lifecycle.ViewModelProvider
import android.content.SharedPreferences
import android.support.v4.app.Fragment
import com.fpt.core.base.CoreActivity
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

abstract class BaseActivity: CoreActivity(), HasSupportFragmentInjector {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>
    @Inject
    lateinit var sharedPreferences: SharedPreferences

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector
}