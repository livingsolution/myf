/*
 * Created by HiepNV12 on 9/10/18 1:13 PM
 * Copyright (c) 2018 by FSOFT. All rights reserved.
 * Last modified 9/10/18 12:28 PM
 */

package com.fpt.myfsoft.base.fragments

import android.arch.lifecycle.ViewModelProvider
import com.fpt.core.base.CoreFragment
import com.fpt.core.di.Injectable
import javax.inject.Inject

abstract class BaseFragment: CoreFragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    abstract fun onError(throwable: Throwable?)
}