/*
 * Created by HiepNV12 on 9/10/18 1:12 PM
 * Copyright (c) 2018 by FSOFT. All rights reserved.
 * Last modified 9/10/18 12:31 PM
 */

package com.fpt.myfsoft.di

import android.app.Application
import com.fpt.core.di.modules.ApiModule
import com.fpt.myfsoft.MyFsoftApplication
import com.fpt.myfsoft.di.module.ActivityBindingModule
import com.fpt.myfsoft.di.module.ApplicationModule
import com.fpt.myfsoft.di.module.DataManagerModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class,
    ApplicationModule::class, ActivityBindingModule::class, ApiModule::class, DataManagerModule::class])
interface ApplicationComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    fun inject(myFsoftApplication: MyFsoftApplication)
}