/*
 * Created by HiepNV12 on 9/10/18 1:12 PM
 * Copyright (c) 2018 by FSOFT. All rights reserved.
 * Last modified 9/10/18 12:56 PM
 */

package com.fpt.myfsoft.di.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.fpt.core.di.ViewModelKey
import com.fpt.core.viewmodel.CoreViewModelFactory
import com.fpt.myfsoft.modules.main.viewmodel.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    abstract fun bindViewModelFactory(factory: CoreViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(mainViewModel: MainViewModel): ViewModel
}