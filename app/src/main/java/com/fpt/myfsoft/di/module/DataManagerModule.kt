/*
 * Created by HiepNV12 on 9/10/18 1:12 PM
 * Copyright (c) 2018 by FSOFT. All rights reserved.
 * Last modified 9/10/18 12:38 PM
 */

package com.fpt.myfsoft.di.module

import android.content.Context
import com.fpt.myfsoft.api.MyFsoftApi
import com.fpt.myfsoft.db.SampleDatabase
import com.fpt.myfsoft.modules.main.room.SampleRoomDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataManagerModule {
    @Provides
    @Singleton
    fun provideMyFsoftApiFactory(): MyFsoftApi {
        return MyFsoftApi()
    }

    @Provides
    @Singleton
    fun provideSampleDatabase(context: Context): SampleRoomDataSource {
        val moviesDatabase = SampleDatabase.getInstance(context)
        return SampleRoomDataSource(moviesDatabase.sampleDao())
    }
}