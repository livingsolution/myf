/*
 * Created by HiepNV12 on 9/10/18 1:08 PM
 * Copyright (c) 2018 by FSOFT. All rights reserved.
 * Last modified 9/10/18 12:59 PM
 */

package com.fpt.myfsoft.di.module

import com.fpt.core.di.modules.CoreApplicationModule
import dagger.Module

@Module(includes = [ViewModelModule::class])
class ApplicationModule : CoreApplicationModule() {

}