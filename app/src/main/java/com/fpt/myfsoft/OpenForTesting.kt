/*
 * Created by HiepNV12 on 9/11/18 3:54 PM
 * Copyright (c) 2018 FSOFT. All rights reserved.
 * Last modified 5/19/18 9:42 PM
 */

package com.fpt.myfsoft

/**
 * This annotation allows us to open some classes for mocking purposes while they are final in
 * release builds.
 */
@Target(AnnotationTarget.ANNOTATION_CLASS)
annotation class OpenClass

/**
 * Annotate a class with [OpenForTesting] if you want it to be extendable in debug builds.
 */
@OpenClass
@Target(AnnotationTarget.CLASS)
annotation class OpenForTesting