/*
 * Created by HiepNV12 on 9/11/18 3:40 PM
 * Copyright (c) 2018 FSOFT. All rights reserved.
 * Last modified 9/11/18 3:40 PM
 */

package com.fpt.myfsoft

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import android.arch.paging.PagedList
import android.arch.paging.RxPagedListBuilder
import com.fpt.myfsoft.api.MyFsoftApi
import com.fpt.myfsoft.db.DATABASE
import com.fpt.myfsoft.db.SampleDatabase
import com.fpt.myfsoft.modules.main.model.SampleResponse
import com.fpt.myfsoft.modules.main.repository.MainRepository
import com.fpt.myfsoft.modules.main.room.SampleDao
import com.fpt.myfsoft.modules.main.room.SampleRoomDataSource
import io.reactivex.Observable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.Mockito.mock

@RunWith(JUnit4::class)
class MainRepositoryTest {
    protected lateinit var repository: MainRepository
    protected val dao = mock(SampleDao::class.java)
    protected lateinit var service : MyFsoftApi
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun init() {
        val db = mock(SampleDatabase::class.java)
        Mockito.`when`(db.sampleDao()).thenReturn(dao)
        Mockito.`when`(db.runInTransaction(ArgumentMatchers.any())).thenCallRealMethod()
        service = MyFsoftApi()
        repository = MainRepository(service, SampleRoomDataSource(dao))
    }

    @Test
    fun loadAPI() {
        val updatedDbData:  MutableList<SampleResponse> = mutableListOf()
        dao.insert(updatedDbData)
        val data = repository.fechSample()
    }
}